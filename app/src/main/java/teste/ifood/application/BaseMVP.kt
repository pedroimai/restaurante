package teste.ifood.application

interface BaseView

interface BasePresenter{
    fun attach(view: BaseView)
    fun detach()
}