package teste.ifood.application

import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import teste.ifood.features.dishes.details.DishDetails
import teste.ifood.features.dishes.details.DishDetailsPresenter
import teste.ifood.features.dishes.details.DishDetailsRepository
import teste.ifood.features.dishes.listing.DishesList
import teste.ifood.features.dishes.listing.DishesListPresenter
import teste.ifood.features.dishes.listing.DishesListRepository

val appModule = module {
    single<RestaurantApi> { RestaurantApiStub() }
    single<SessionStorage> { InMemorySessionStorage() }
}

private fun api(): RestaurantApi {
    val retrofit = Retrofit.Builder()
        .baseUrl("https://...")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    return retrofit.create(RestaurantApi::class.java)
}

val dishDetailsModule = module {
    factory<DishDetails.Source> { DishDetailsRepository(get(), get()) }
    factory<DishDetails.Presenter> { DishDetailsPresenter(get()) }
}

val dishesListModule = module {
    factory<DishesList.Source> { DishesListRepository(get(), get()) }
    factory<DishesList.Presenter> { DishesListPresenter(get()) }
}