package teste.ifood.application

class InMemorySessionStorage: SessionStorage {
    override var selectedDish: String? = null
    override var lat: Double? = null
    override var lon: Double? = null
    override fun clear() {
        selectedDish = null
        lat = null
        lon = null
    }
}

interface SessionStorage{
    var selectedDish:String?
    var lat:Double?
    var lon:Double?
    fun clear()
}

