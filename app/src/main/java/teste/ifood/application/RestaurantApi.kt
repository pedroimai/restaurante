package teste.ifood.application

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import teste.ifood.features.dishes.details.DishDetailsPayload

const val HEADER_CONTENT_JSON = "Content-Type: application/json"


interface RestaurantApi {
    @Headers(HEADER_CONTENT_JSON)
    @GET("dish-list/{lat}/{lon}")
    fun getDishesList(@Path("lat") lat: Double, @Path("lon") lon: Double): Observable<Response<List<String>>>

    @Headers(HEADER_CONTENT_JSON)
    @GET("dish-detail/{dish}")
    fun getDishDetailsByName(@Path("dish") name: String): Observable<Response<DishDetailsPayload>>
}

class RestaurantApiStub : RestaurantApi {
    private val mockedDishesNameList = listOf("Prato 1", "Prato 2", "Prato 3", "Prato 4", "Prato 5")
    private val mockedDishesList = listOf(
        DishDetailsPayload(mockedDishesNameList[0], "Restaurante 1"),
        DishDetailsPayload(mockedDishesNameList[1], "Restaurante 2"),
        DishDetailsPayload(mockedDishesNameList[2], "Restaurante 3"),
        DishDetailsPayload(mockedDishesNameList[3], "Restaurante 4"),
        DishDetailsPayload(mockedDishesNameList[4], "Restaurante 5")
    )

    override fun getDishesList(lat: Double, lon: Double): Observable<Response<List<String>>> =
        Observable.just(Response.success(mockedDishesNameList))

    override fun getDishDetailsByName(name: String): Observable<Response<DishDetailsPayload>> =
        Observable.just(Response.success(mockedDishesList.first { it.name == name }))

}

