package teste.ifood.application

import android.app.Application
import org.koin.android.ext.android.startKoin

class RestaurantApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(
            this, listOf(
                appModule,
                dishDetailsModule,
                dishesListModule
            )
        )
    }
}