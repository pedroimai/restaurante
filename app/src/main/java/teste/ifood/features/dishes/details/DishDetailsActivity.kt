package teste.ifood.features.dishes.details

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.dish_details_activity.*
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import teste.ifood.R

class DishDetailsActivity : AppCompatActivity(), OnMapReadyCallback, DishDetails.View {
    private val presenter: DishDetails.Presenter by inject()
    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dish_details_activity)
        presenter.attach(this)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.isMyLocationEnabled = true
        presenter.fetchDetails()

    }

    override fun showDishDetails(model: Dish) {
        txt_dish_name.text = model.name
        txt_restaurante_name.text = model.restaurant

        val pin = LatLng(model.lat, model.lon)
        mMap.addMarker(MarkerOptions().position(pin).title("Restaurante"))
        mMap.moveCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.builder()
                    .target(LatLng(model.lat, model.lon))
                    .zoom(15f)
                    .bearing(0f)
                    .tilt(0f)
                    .build()
            )
        )
    }

    override fun showError() {
        toast("Ops, ocorreu um erro :/")
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }
}
