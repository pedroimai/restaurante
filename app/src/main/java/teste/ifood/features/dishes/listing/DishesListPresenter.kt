package teste.ifood.features.dishes.listing

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import teste.ifood.application.BaseView

class DishesListPresenter(
    private val source: DishesList.Source,
    private val subs: CompositeDisposable = CompositeDisposable()
) : DishesList.Presenter {

    lateinit var view: DishesList.View
    override fun fetchDishesByLatLon(lat: Double, lon: Double) {
        subs.clear()
        subs.add(
            source.getDishesByLatLon(lat, lon)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { view.showDishes(it) },
                    { view.showError() }
                )
        )
    }

    override fun select(dish: String) {
        source select dish
        view.openDetails()
    }

    override fun attach(view: BaseView) {
        this.view = view as DishesList.View
    }

    override fun detach() {
        subs.clear()
    }
}

