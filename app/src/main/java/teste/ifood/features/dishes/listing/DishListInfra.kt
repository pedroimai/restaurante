package teste.ifood.features.dishes.listing

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import teste.ifood.application.RestaurantApi
import teste.ifood.application.SessionStorage

class DishesListRepository(
    private val api: RestaurantApi,
    private val sessionStorage: SessionStorage
) : DishesList.Source {
    override fun getDishesByLatLon(lat: Double, lon: Double): Observable<List<String>> {
        sessionStorage.lat = lat
        sessionStorage.lon = lon
        return api.getDishesList(lat, lon)
            .subscribeOn(Schedulers.io())
            .filter { it.isSuccessful }
            .map { it.body() }
    }

    override fun select(dish: String) {
        sessionStorage.selectedDish = dish
    }

}