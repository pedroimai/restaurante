package teste.ifood.features.dishes.details

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import teste.ifood.application.BaseView


class DishDetailsPresenter(
    private val source: DishDetails.Source,
    private val subs: CompositeDisposable = CompositeDisposable()
) :
    DishDetails.Presenter {

    lateinit var view: DishDetails.View

    override fun fetchDetails() {
        subs.clear()
        subs.add(source.getDetails()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { view.showDishDetails(it) },
                { view.showError() }
            ))
    }


    override fun attach(view: BaseView) {
        this.view = view as DishDetails.View
    }

    override fun detach() {
        subs.clear()
    }
}