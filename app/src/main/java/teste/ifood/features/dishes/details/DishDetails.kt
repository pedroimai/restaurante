package teste.ifood.features.dishes.details

import io.reactivex.Observable
import teste.ifood.application.BasePresenter
import teste.ifood.application.BaseView

interface DishDetails {
    interface View : BaseView {
        fun showDishDetails(model: Dish)
        fun showError()
    }

    interface Presenter : BasePresenter {
        fun fetchDetails()

    }

    interface Source {
        fun getDetails(): Observable<Dish>
    }
}

data class Dish(
    val name: String = "",
    val restaurant: String = "",
    val lat: Double = 0.0,
    val lon: Double = 0.0
)