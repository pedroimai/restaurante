package teste.ifood.features.dishes.details

import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import teste.ifood.application.RestaurantApi
import teste.ifood.application.SessionStorage


class DishDetailsRepository(
    private val api: RestaurantApi,
    private val sessionStorage: SessionStorage
) : DishDetails.Source {
    @SuppressLint("CheckResult")
    override fun getDetails(): Observable<Dish> =
        api.getDishDetailsByName(sessionStorage.selectedDish.orEmpty())
            .subscribeOn(Schedulers.io())
            .filter { (it.isSuccessful) }
            .map { it.body()?.toDish(sessionStorage.lat ?: 0.0, sessionStorage.lon ?: 0.0) }
}

data class DishDetailsPayload(
    @SerializedName("dish") val name: String? = "",
    @SerializedName("restaurant") val restaurant: String? = ""
) {
    fun toDish(lat: Double, lon: Double) = Dish(
        name = name.orEmpty(),
        restaurant = restaurant.orEmpty(),
        lat = lat,
        lon = lon
    )
}
