package teste.ifood.features.dishes.listing

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.BackpressureStrategy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dishes_list_activity.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider
import teste.ifood.R
import teste.ifood.features.dishes.details.DishDetailsActivity

class DishesListActivity : AppCompatActivity(), DishesList.View {
    private val presenter: DishesList.Presenter by inject()
    private val rxPermissions by lazy { RxPermissions(this) }
    private val locationProvider by lazy { ReactiveLocationProvider(this) }
    private val listAdapter by lazy { DishesListAdapter { presenter select it } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dishes_list_activity)
        presenter.attach(this)
        requestPermission()
        initDishesList()
    }

    override fun showDishes(dishes: List<String>) {
        listAdapter.addAll(dishes)
    }

    override fun openDetails() {
        startActivity<DishDetailsActivity>()
    }

    override fun showError() {
        toast("Ocorreu um erro :/")
    }

    private fun initDishesList() {
        recycler_dishes_list.run {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = listAdapter
        }
    }

    @SuppressLint("MissingPermission", "CheckResult")
    private fun requestLocation() {
        locationProvider.lastKnownLocation
            .subscribeOn(Schedulers.io())
            .toFlowable(BackpressureStrategy.LATEST)
            .firstOrError()
            .subscribe({ location ->
                presenter.fetchDishesByLatLon(location.latitude, location.longitude)
            }, { toast("Ocorreu um erro :/") }
            )
    }

    @SuppressLint("CheckResult")
    private fun requestPermission() {
        rxPermissions.requestEach(
            Manifest.permission.ACCESS_FINE_LOCATION
        ).subscribe { permission ->
            when {
                permission.granted -> requestLocation()
                permission.shouldShowRequestPermissionRationale -> toast("A permissão foi negada :/")
                else -> toast("Permissão negada para sempre T___T")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }
}