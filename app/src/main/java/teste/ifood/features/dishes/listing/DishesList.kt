package teste.ifood.features.dishes.listing

import io.reactivex.Observable
import teste.ifood.application.BasePresenter
import teste.ifood.application.BaseView

interface DishesList {
    interface View : BaseView {
        fun showDishes(dishes: List<String>)
        fun openDetails()
        fun showError()
    }

    interface Presenter : BasePresenter {
        fun fetchDishesByLatLon(lat: Double, lon: Double)
        infix fun select(dish: String)
    }

    interface Source {
        fun getDishesByLatLon(lat: Double, lon: Double): Observable<List<String>>
        infix fun select(dish: String)
    }
}