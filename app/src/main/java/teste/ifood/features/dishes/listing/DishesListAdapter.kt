package teste.ifood.features.dishes.listing

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dish_list_item.view.*
import teste.ifood.R

class DishesListAdapter(private val itemClick: (String) -> Unit) :
    RecyclerView.Adapter<DishesListAdapter.ViewHolder>() {
    private var dishes = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            layoutInflater.inflate(
                R.layout.dish_list_item,
                parent,
                false
            ), itemClick
        )
    }

    override fun getItemCount(): Int {
        return dishes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dishes[position])
    }

    class ViewHolder(view: View, val itemClick: (String) -> Unit) : RecyclerView.ViewHolder(view) {

        fun bind(dishName: String) {
            with(itemView) {
                dish_name.text = dishName
                setOnClickListener { itemClick(dishName) }
            }
        }
    }

    fun addAll(dishes: List<String>) {
        this.dishes.addAll(dishes)
        notifyDataSetChanged()
    }
}